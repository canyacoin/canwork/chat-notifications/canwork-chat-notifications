
# CanWork chat email notifications

Firebase scheduled function to send out email notifications for unread chat messages.

Loosely based on [Go chat notifications](https://gitlab.com/canyacoin/canwork/services/chat-notifications).

## Versions of used tools

- node v10.22.0
- npm v6.14.7
- firebase-tools v8.9.2
- firebase-admin v8.10.0
- firebase-functions 3.6.1
- sendgrid/mail 7.3.0

