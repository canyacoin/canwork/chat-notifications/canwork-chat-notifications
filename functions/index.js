const functions = require('firebase-functions');
const env = functions.config();
const firebaseAdmin = require('firebase-admin');
const firebaseRef = firebaseAdmin.initializeApp();
const db = firebaseAdmin.firestore();
const sgMail = require('@sendgrid/mail')
  

async function sendEmail(name, email, userId) {
  const senderAddress = "support@canya.com";
  const senderName = "CanYa support";
  const subject = "You have unread chat messages";
  const title = "You have unread chat messages on CanWork";
  const body = "People on CanWork are waiting for your response!";
  const returnLinkText = "Visit CANWork";
  const returnLinkUrl = "https://app.canwork.io/inbox/chat";
  
  const templateId = env.chatnotifications.sendgridtemplateid;
  const apiKey = env.chatnotifications.sendgridapikey;
  sgMail.setApiKey(apiKey);
  const msg = {
    to: {
      name,
      email
    },
    from: {
      name: senderName,
      email: senderAddress
    },
    templateId: templateId,
    dynamicTemplateData: {
      subject,
      title,
      body,
      returnLinkText,
      returnLinkUrl
    }
  };
  try {
    await sgMail.send(msg);
    return true;
    
  } catch (error) {
    
    // Log friendly error
    functions.logger.error(`chatEmailNotifications: failed to send email to  user ${userId}: ${error.message}`);
    return false;

  }  
  
}

exports.chatEmailNotifications = functions.pubsub.schedule('every 1 hours').onRun(async (context) => {
  const notificationsCollection = db.collection('notifications');
  const usersCollection = db.collection('users');
  
  const notificationsSnapshot = await notificationsCollection.where("chat", "==", true).get();
  let notifications = [];
  if (!notificationsSnapshot.empty) {
    notificationsSnapshot.forEach((doc) => {
        notifications.push(doc.id);
    });            
  }
  let sent = 0;
  let flagged = 0;
  for (const userId of notifications) {  
    const userSnapshot = await usersCollection.doc(userId).get();
    if (userSnapshot.exists) {
      const user = userSnapshot.data();
      let sentEmail = await sendEmail(user.name, user.email, userId);
      if (sentEmail) sent++;
      
      // mark as done anyway
      await notificationsCollection.doc(userId).update({chat:false});
      flagged++;
      
      
    } else {
        functions.logger.error(`chatEmailNotifications: failed to retrieve user ${userId}`);
      
    }
    
  }
  
  functions.logger.info(`chatEmailNotifications: found ${notifications.length}, sent ${sent}, flagged ${flagged}`);
  return null;
});
